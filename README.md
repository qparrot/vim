<!-- **************************************************************************** -->
<!--                                                                              -->
<!--                                                      ::::::::    :::::::::   -->
<!-- README.md                                          :+:    :+:   :+:    :+:   -->
<!--                                                   +:+    +:+   +:+    +:+    -->
<!-- By: quentin <quentin.parrot@protonmail.com>      +#+    +:+   +#++:++#+      -->
<!--                                                 +#+    +#+   +#+             -->
<!-- Created: 2022/10/22 09:36:26 by quentin        #+#    #+#   #+#              -->
<!-- Updated: 2022/10/22 09:56:10 by quentin        ########### ###               -->
<!--                                                                              -->
<!-- **************************************************************************** -->

# VIM HEADER

This script add a header feature bind to a specific key. The header mention the name of the file, the date of creation, date of last modification and usser name and email.

## REQUIREMENTS

1. A machine running with linux.

## INSTALLATION


1. If necessary, create a folder "plugin" in "~/.vim".
2. Copy stdheader in the newly created "plugin" folder.
3. Change the ASCII art at line 1, by using this [Text To Ascii Art convertor](https://www.texttool.com/ascii-font#p=display&h=1&v=0&f=Alligator&t=QP)
4. Change the mail adresse at line 91.

## CODE EXPLANATION

1. From line 1 to 10, we have the variable ASCIIART value that stores our ascii art sequence.
2. From line 11 to 15, we have different variables changing the header construction.
3. From line 17 to 34, we have a dictionnary pairing types of file with right commenting caracters.
4. on line 36 the "filetype" function changes the "start", "end" and "fill" character to match the right one based on the types dictionnary.
5. On line 56 the "textline" function creates a line.
6. On line 62 the "line" function creates lines with information: name, etc based on the line value.
7. From line 80 to 106 each functions retrieve a specific data.
8. The function "insert" the line forming the header.
9. From the line 121 the function "update" updates the line 9.
10. On line 133 the function "stdheader" checks for update and call "insert".
11. line 140, we create a new command on vim to implement the creation of the header.
12. line 141, we bind the new command to a macro.
13. line 142, we ask vim to call "update" before opening the file.

## RESOURCES

1. https://github.com/pbondoer/vim-42header
2. https://www.texttool.com/ascii-font#p=display&h=1&v=0&f=Alligator&t=QP
